package fr.ulille.iutinfo.banque;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GestureDetectorCompat;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class VueListe extends AppCompatActivity {

    private int[] soldes;
    private String[] joueurs;
    private TextView labelHistorique;

    class FlingDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
            if ((velocityX > velocityY) && (velocityX > 0)) {
                finish();
                return true;
            }
            return false;
        }
    }

    private GestureDetectorCompat mDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vue_liste);

        joueurs = getResources().getStringArray(R.array.nom_joueurs);

        // TODO Querestion 2.7 cimmunications entre activités

        // TODO Questino 2.8 Construction de la listView

        mDetector = new GestureDetectorCompat(this, new VueListe.FlingDetector());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.mDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }
}
