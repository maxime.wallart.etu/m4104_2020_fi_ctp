package fr.ulille.iutinfo.banque;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GestureDetectorCompat;

import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    public  static final String SOLDES_KEY = "Soldes";

    Spinner donneur;
    Spinner receveur;
    EditText montant;
    EditText operation;

    private String banque;
    private String joueurs[];
    private int soldeInitial;

    private int[] soldes;

    private class FlingDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
            if ((Math.abs(velocityX) > Math.abs(velocityY)) && (velocityX < 0)) {

                // TODO Question 2.6 Lancement de la seconde activité

                // TODO Question 2.7 Communication entre activités

                return true;
            }
            return false;
        }
    }

    private GestureDetectorCompat mDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // TODO Question 2.1 Chargement des ressources

        // TODO Question 2.2 Affichage des noms des joueurs

        // TODO Question 2.5 Changement de configuration

        donneur = findViewById(R.id.donneur);
        receveur = findViewById(R.id.receveur);
        operation = findViewById(R.id.operation);
        montant = findViewById(R.id.montant);

        // TODO Question 2.3 Initialisation des spinners

        mDetector = new GestureDetectorCompat(this, new FlingDetector());

        update();
    }

    private void update() {
        // TODO Question 2.2 Affichage des soldes des joueurs
    }

    public void ajoute_operation(View view) {
        // TODO Question 2.4 ajout d'une transaction
        update();
    }

    // TODO Question 2.5 Changement de configuration

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.mDetector.onTouchEvent(event)) {
            return true;
        }
        return super.onTouchEvent(event);
    }
}
